import java.io.*;
import java.util.ArrayList;

public class FileUtils {

    static ArrayList<String> dataRows = new ArrayList<>();
    static ArrayList<String> checkDataRows = new ArrayList<>();

    /**
     * Метод для загрузки первых 2х строк из csv файла
     */
    public void loadCSVFileForCheck(String inputFile) {

        BufferedReader bufferedReader;

        try {
            bufferedReader = new BufferedReader(new FileReader(inputFile));
            String line;

            int counter = 0;
            while (counter < 2) { // считаем заголовок и первую строку данных для проверки формата
                line = bufferedReader.readLine();
                checkDataRows.add(line);
                counter++;
            }
            bufferedReader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Метод для загрузки всех данных из csv файла
     */
    public void loadCSVFile(String inputFile) {

        BufferedReader bufferedReader = null;

        try {
            bufferedReader = new BufferedReader(new FileReader(inputFile));
            String line;

            // цикл для считывания всех строк по одной строке
            while ((line = bufferedReader.readLine()) != null) {
                dataRows.add(line);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * Метод для записи в sql файл(опционально его можно вызвать в Main) -
     * позволяет увидеть как прошлипреобразования в sql формат
     */
    public void writeInSQlFile(String outputFile){

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));

            writer.write(Parser.queryString);

            writer.close(); // можно закрыть поток здесь

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Couldn't write");

        }
    }


    /**
     * Метод для записи в csv файл - позволяет записывать полученный результат поиска в выходной CSV файл
     */
    public void writeInCSVFile(String[][] result, String outputFile){

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));

            for (int i=0; i<result.length; i++){
                for (int j=0; j<result[i].length; j++){
                    writer.write(result[i][j] + "  ");
                }
                writer.write("\n");
            }

            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Couldn't write");
        }
    }
}
