import java.sql.Connection;



public class Main {

    public static void main(String[] args) throws Exception{

        String input = args[1], output = args[3], column_name = args[7], findExpression = args[9];

        //проверка введенных данных и входного файла
        FileUtils fileUtils = new FileUtils();
        fileUtils.loadCSVFileForCheck(input);
        InputChecker inputChecker = new InputChecker();
        if(!(inputChecker.checkInputFile())) System.exit(0);
        if(!(inputChecker.checkOutputFile(output))) System.exit(0);
        if(!(inputChecker.checkColumn(column_name))) System.exit(0);
        if(!(inputChecker.checkExpression(findExpression))) System.exit(0);


        // Считаем данные с файла
        fileUtils.loadCSVFile(input);


        // Преобразуем данные из формата CSV в SQL
        Parser parser = new Parser();
        parser.parseCSVFile();
        parser.convertCSVtoSQL();


        // Запишем данные в БД и выберем необходимые нам данные
        DBCommunication.init();
        Connection connection =  DBCommunication.getConnection(); // зачем сюда передавать connection, если можно внутри класса создать переменную экземпляра?
        DBCommunication.statements(connection);
        String[][] result = DBCommunication.resultSet(connection, column_name, findExpression);
        fileUtils.writeInCSVFile(result, output);

    }


}
