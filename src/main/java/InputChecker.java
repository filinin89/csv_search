import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class InputChecker {

    private String[] dataHeaders1;
    private ArrayList<String> validDataTypes = new ArrayList<>();
    private String findColumnType;


    /**
     * Метод для проверки формата входного файла -
     * обрабатывает множество различных ошибок ввода и сообщает пользователю для избежания некорректного выполнения программы
     * ПРОВЕРЯЕТ ПЕРВЫЕ ДВЕ СТРОЧКИ, чтоб проверка больших файлов не занимала много времени.
     * Проверки формата двух строчек будет достаточно, чтоб проверить верно ли набираются строчки, остальные уже можно набрать по подобию правильным.
     */
    public boolean checkInputFile() {
        try {
            String[] checkSemicolon;
            String[] dataHeaders;
            String[] dataRow;
            String[] dataRow1;
            boolean checkDataType = true;
            String[] dataHeadersTypes;

            validDataTypes.add("String");
            validDataTypes.add("Date");
            validDataTypes.add("Integer");
            validDataTypes.add("Float");

            // общая проверка
            // проверка наличия ';' как разделителя везде и в заголовке и в первом ряду
            for (int i = 0; i < 2; i++) { // нам нужно проверить только заголовок и первую строку
                checkSemicolon = FileUtils.checkDataRows.get(i).split("[;][^\";]"); // не делим по ; внутри слова // можно попробовать matches()
                if (checkSemicolon.length == 1) {
                    System.out.println("Вам нужно использовать символ ';' в качестве разделителя. Измените файл и запустите программу повторно");
                    return false;
                }
            }

            // это более точная проверка
            // проверка разделителя после типа данных в заголовке
            dataHeaders = FileUtils.checkDataRows.get(0).split(" ");
            for(int i = 0; i< dataHeaders.length-1; i++) { // последний эл без разделителя
                if (i % 2 != 0) {
                    if ((dataHeaders[i].charAt(dataHeaders[i].length() - 1)) != ';') {
                        System.out.println("Вам нужно использовать символ ';' в качестве разделителя в объявлении колонок. Измените файл и запустите программу повторно");
                        return false;
                    }
                }
            }
            // проверка разделителя в ряду между каждым элементом
            dataRow = FileUtils.checkDataRows.get(1).split(" ");
            for(int i = 0; i< dataRow.length-1; i++) {
                if ((dataRow[i].charAt(dataRow[i].length() - 1)) != ';') {
                    System.out.println("Вам нужно использовать символ ';' в качестве разделителя между элементами ряда. Измените файл и запустите программу повторно");
                    return false;
                }
            }


            dataHeaders1 = FileUtils.checkDataRows.get(0).replace(";", "").replace(",", "").split(" "); // этот случай работает даже если ';' мы поставим через пробелы
            dataHeadersTypes = new String[dataHeaders1.length];
            // проверка типов данных на том, что они стоят на своих местах
            for (int i = 0, j = 0; i < dataHeaders1.length; i++) {
                if (i % 2 != 0) { // на всех нечетных местах должны быть типы данных
                    checkDataType = validDataTypes.contains(dataHeaders1[i]);
                    dataHeadersTypes[j] = dataHeaders1[i]; // сохраним тип данных
                    j++;
                    if (!checkDataType) {
                        System.out.println("Вы ввели неверный тип данных, либо забыли его ввести ");
                        return false;
                    }
                }
            }


            // проверяем соответствие данных в ряду типам данных в заголовке и проверим сам формат данных
            dataRow1 = FileUtils.checkDataRows.get(1).replace(" ", "").split(";");
            for (int i = 0; i < dataRow1.length; i++) {

                switch (dataHeadersTypes[i]) { // типы данных в этом массиве идут соответствующе каждому элементу в ряду

                    case "String":
                        break; // здесь все впорядке

                    case "Date":
                        if (!(isNumber(dataRow1[i].replace(".", "")))) { // проверка на числа в дате
                            System.out.println("Вы ввели не число в " + (i+1) + "-й колонке с типом - дата, попробуйте еще раз");
                            return false;
                        }
                        String[] dateCheck = new String[3];
                        if(!(dataRow[i].contains("."))) {
                            System.out.println("Вы ввели просто число, не дату в " + (i+1) + "-й колонке с типом - дата, попробуйте еще раз");
                            return false;
                        }
                        char[] charData = dataRow[i].toCharArray();
                        if((charData[2] != '.') && (charData[5] != '.')){
                            System.out.println("Вы ввели дату в неверном формате в " + (i+1) + "-й колонке с типом - дата, нужный формат dd.mm.yyyy, попробуйте еще раз");
                            return false;
                        }

                        dateCheck = dataRow1[i].replace(".", ",").split(",");
                        int dayCheck = Integer.parseInt(dateCheck[0]);
                        int monthCheck = Integer.parseInt(dateCheck[1]);
                        if (!(1 <= dayCheck && dayCheck <= 31)) {
                            System.out.println("Вы ввели некорректный день в " + (i+1) + "-й колонке с типом - дата, попробуйте еще раз");
                            return false;
                        }
                        if (!(1 <= monthCheck && monthCheck <= 12)) {
                            System.out.println("Вы ввели некорректный месяц в " + (i+1) + "-й колонке с типом - дата, попробуйте еще раз");
                            return false;
                        }
                        break;

                    case "Integer":
                        if (!(isNumber(dataRow1[i]))) {
                            System.out.println("Вы ввели не число в " + (i+1) + "-й колонке с типом - Integer, попробуйте еще раз");
                            return false;
                        }
                        break;


                    case "Float":
                        boolean result = false;
                        String[] floatParts;
                        int index = dataRow1[i].indexOf(',');
                        if (index != -1) {
                            floatParts = dataRow1[i].split(",");
                            for(int j=0; j<floatParts.length; j++){
                                result = isNumber(floatParts[j]); // если хоть в одной из частей окажется не число
                                if(!result){
                                    System.out.println("Вы ввели не число в " + (i+1) + "-й колонке с типом - Float, попробуйте еще раз");
                                    return false;
                                }
                            }
                        } else {
                            System.out.println("Вам нужно использовать ',' в кач-ве разделителя целой и дробной части в " + (i+1) + "-й колонке с типом - Float, попробуйте еще раз");
                            return false;
                        }
                        break;
                }

            }

        } catch (Exception e){
            System.out.println("Вы допустили ошибку в синтаксисе. Исправьте файл и запустите программу еще раз");
        }

        System.out.println("\nВаш входной файл успешно прошел проверку!");
        return true;
    }

    public boolean isNumber(String str) {
        boolean result = true;
        for (int i = 0; i < str.length(); i++) {
            if(!(Character.isDigit(str.charAt(i)))){
                result = false;
                break;
            }
        }
        return result;

    }

    /**
     * Метод для проверки правильности пути выходного файла
     */
    public boolean checkOutputFile(String output){
        boolean result = true;

        // проверка каталога
        Path path = Paths.get(output);
        if (Files.notExists(path)) {
            System.out.println("Такого католога не существует, проверьте путь до выходного файла" );
            result = false;
        }

        return result;
    }


    /**
     * Метод для проверки наличия вводимой колонки
     */
    public boolean checkColumn(String column){
        boolean result = false;
        for(int i=0; i<dataHeaders1.length; i++){
            if(dataHeaders1[i].equals(column)){
                result = true;
                findColumnType = dataHeaders1[i+1];
                break;
            }
        }
        if(!result) System.out.println("Вы ввели неверную колонку для поиска");
        return result;
    }

    /**
     * Метод для проверки введенного выражения на соответствие типу данных колонки -
     * определяет является ли введенное выражение каким-либо из типов данных и указывает на ошибки ввода
     */
    public boolean checkExpression(String expression){

        // этот код почти идентичен коду в проверке входного файла, в следующей версии вынесу в один метод и адаптирую этот код под оба варианта
        // проверим, соотвтствует ли введеное значение типу данных в колонке
        try {

            switch (findColumnType) { // типы данных в этом массиве идут соответствующе каждому элементу в ряду

                case "String":
                    break; // здесь все впорядке

                case "Date":
                    if (!(isNumber(expression.replace(".", "")))) { // проверка на числа в дате
                        System.out.println("Вы ввели не число при вводе выражения для поиска");
                        return false;
                    }
                    String[] dateCheck = new String[3];
                    if (!(expression.contains("."))) {
                        System.out.println("Вы ввели просто число, не дату при вводе выражения для поиска");
                        return false;
                    }
                    char[] charData = expression.toCharArray();
                    if ((charData[2] != '.') && (charData[5] != '.')) {
                        System.out.println("Вы ввели дату в неверном формате при вводе выражения для поиска, нужный формат dd.mm.yyyy");
                        return false;
                    }
                    dateCheck = expression.replace(".", ",").split(",");
                    int dayCheck = Integer.parseInt(dateCheck[0]);
                    int monthCheck = Integer.parseInt(dateCheck[1]);
                    if (!(1 <= dayCheck && dayCheck <= 31)) {
                        System.out.println("Вы ввели некорректный день при вводе выражения для поиска");
                        return false;
                    }
                    if (!(1 <= monthCheck && monthCheck <= 12)) {
                        System.out.println("Вы ввели некорректный месяц при вводе выражения для поиска");
                        return false;
                    }
                    break;

                case "Integer":
                    if (!(isNumber(expression))) {
                        System.out.println("Вы ввели не число при вводе выражения для поиска");
                        return false;
                    }
                    break;


                case "Float":
                    boolean result = false;
                    String[] floatParts;
                    int index = expression.indexOf(',');
                    if (index != -1) {
                        floatParts = expression.split(",");
                        for (int j = 0; j < floatParts.length; j++) {
                            result = isNumber(floatParts[j]); // если хоть в одной из частей окажется не число
                            if (!result) {
                                System.out.println("Вы ввели не число при вводе выражения для поиска");
                                return false;
                            }
                        }
                    } else {
                        System.out.println("Вам нужно использовать ',' в кач-ве разделителя целой и дробной части ");
                        return false;
                    }
                    break;
            }
        }catch(Exception e) {
            System.out.println("Вы допустили ошибку в синтаксисе. Исправьте вводимые данные и запустите программу еще раз");
        }


          return true;
    }





}
