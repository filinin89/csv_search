import java.sql.*;

public class DBCommunication {


    public static void init() throws ClassNotFoundException {
        Class.forName("org.h2.Driver");
    }

    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:h2:mem:test");
    }

    // заполняем данные
    public static void statements(Connection connection) throws SQLException {
        try (Statement statement = connection.createStatement()) { // более короткая версия с автоматическим закрытием потоков
            statement.execute(Parser.queryString);                            // создана таблица и заполнена значениями
        }
    }

    // берем данные
    public static String[][] resultSet(Connection connection, String column_name, String findExpression) throws SQLException {

        // Используем Prepared Statement
        System.out.println("\nВыборка\n----------------");

        PreparedStatement pstmt = connection.prepareStatement("select * from persons WHERE " + column_name + " =  ? ", ResultSet.TYPE_SCROLL_INSENSITIVE,
                ResultSet.CONCUR_UPDATABLE); // сделаем Result Set прокручиваемым, чтоб можно было узнать его размер способом ниже

        char[] findExpressionSeq = findExpression.toCharArray();

        // если дата
        if((findExpressionSeq[2] == '.') && (findExpressionSeq[5] == '.')) { // мы можем понять наша дата это или не дата, т.к. у нас везде формат dd.mm.yyyy

            // подготовим под конвертацию в sqlDate
            String day = findExpression.substring(0,2); String month = findExpression.substring(2,6); String year = findExpression.substring(6,10);
            String findExpressionRet = year + month + day;
            findExpressionRet = findExpressionRet.replace(".", "-");

            java.sql.Date sqlDate = java.sql.Date.valueOf(findExpressionRet);
            pstmt.setDate(1, sqlDate);

        }else{
            if(isValueFloat(findExpression)){ // если Float
                findExpression = findExpression.replace(",", ".");
            }
            pstmt.setString(1, findExpression);
        }

        ResultSet rs = pstmt.executeQuery(); //сюда мы получим все соответсвующие выборке ряды

        //определим размер массива для записи найденных значений
        //выделится столько места, сколько раз это значение встретилось встретилось при проходе по колонке
        rs.last();
        int rsSize = rs.getRow();
        rs.beforeFirst();
        ResultSetMetaData metaData = rs.getMetaData();
        int numberOfColumns = metaData.getColumnCount();
        String[][] result = new String[rsSize][numberOfColumns]; // используем массив, чтоб выводить в файл несколько значений, потому что по одному запросу может найтись несколько записей

        int rowCounter=0;
        while (rs.next()) { // здесь переключаемся на следующий ряд

            for(int i=0; i<numberOfColumns; i++) { // здесь переключаемся по столбцам в ряду

                result[rowCounter][i] = rs.getString(i+1); // выведем весь ряд последовательно //i+1 потому что значения в Result Set хранятся, начиная с 1

                if (isValueFloat(result[rowCounter][i])) {
                    result[rowCounter][i] = result[rowCounter][i].replace(".", ","); // выведем запятую
                }

                // если искали дату - переведем ее в наш формат
                char[] resultSeq = result[rowCounter][i].toCharArray();
                if (resultSeq.length >= 10 && (resultSeq[4] == '-' && resultSeq[7] == '-')) {
                    String year = result[rowCounter][i].substring(0, 4);
                    String month = result[rowCounter][i].substring(4, 8);
                    String day = result[rowCounter][i].substring(8, 10);
                    result[rowCounter][i] = day + month + year;
                    result[rowCounter][i] = result[rowCounter][i].replace("-", ".");
                }
                System.out.print(result[rowCounter][i] + "  "); // выведем элемент в консоль
            }
            System.out.println();
            rowCounter++;
        }

        System.out.println("----------------");

        return result;
    }


    public static boolean isValueFloat(String value){
        // отличаем Float с запятой от комментария с запятой
        final String digits = "123456789";
        char[] valueSeq = value.toCharArray();

        int numbCounter=0;
        for(int i=0; i<valueSeq.length; i++){
            if(digits.contains(String.valueOf(valueSeq[i]))) numbCounter++;
        }
        if(numbCounter == valueSeq.length-1){ // если все символы, кроме запятой, - числа, то это Float, у Int кол-во символов будет ровно кол-ву чисел
            return true;
        } else{
            return false;
        }
    }


}
