
import java.util.ArrayList;

public class Parser {

    private String[] dataHeaders;
    private ArrayList<String[]> dataCells = new ArrayList<>();
    static String queryString; // можно сделать его приватным и геттер для него


    /**
     * Метод для синтаксического разбора csv файла на отдельные элементы
     */
    public void parseCSVFile() {

        String hDelimiter = " "; // будем делить по пробелу все названия столбцов и их типы, так как нам все равно надо будет разбивать тип и названия поля
        String delimiter = "; ";
        String[] arrDataCells;

        // цикл для разбиения строк на отдельные эл
        for (int i = 0; i < FileUtils.dataRows.size(); i++) {

            if (i < 1) {
                dataHeaders = FileUtils.dataRows.get(i).replace(";", "").replace(",", "").replace("-", "_").split(hDelimiter);
            } else {
                arrDataCells = FileUtils.dataRows.get(i).replace("-", "_").split(delimiter); //разделяем строку на отдельные элементы и запишем их во временный массив
                dataCells.add(arrDataCells); // добавляем этот массив в arrayList
            }
        }
    }


    /**
     * Метод для преобразования разобранных данных в формат sql
     */
    public void convertCSVtoSQL() {

        String[] dataHeadersTypes;
        String[] dataHeadersSqlTypes;
        String[] dataHeadersFields;
        String[] dataHeaders2;
        ArrayList<Integer> whereIsDate = new ArrayList<>();
        ArrayList<Integer> whereIsFloat = new ArrayList<>();
        String wholeHeader, wholeHeaderFields, wholeCells;

        // дополнительно подготовим заголовок - разобьем типы данных и поля в разные массивы. Это необходимо для разных sql операций
        dataHeadersTypes = new String[dataHeaders.length / 2];
        dataHeadersFields = new String[dataHeaders.length / 2];
        for (int i = 0, j = 0, v = 0; i < dataHeaders.length; i++) {
            if (i % 2 == 0) {
                dataHeadersFields[v] = dataHeaders[i];
                v++;
            } else {
                dataHeadersTypes[j] = dataHeaders[i];
                j++;
            }
        }

        // "переведем" типы даных Java в соответствующие SQL
        dataHeadersSqlTypes = new String[dataHeadersTypes.length];
        for (int i = 0; i < dataHeadersTypes.length; i++) {
            switch (dataHeadersTypes[i]) {
                case "String":
                    dataHeadersSqlTypes[i] = "VARCHAR";
                    break;
                case "Date":
                    dataHeadersSqlTypes[i] = "DATETIME";
                    whereIsDate.add(i);
                    break;
                case "Integer":
                    dataHeadersSqlTypes[i] = "INT";
                    break;
                case "Float":
                    dataHeadersSqlTypes[i] = "DEC";
                    whereIsFloat.add(i);
                    break;
            }

        }

        // объединим массивы в пару поле-тип для создания таблицы CREATE TABLE
        dataHeaders2 = new String[dataHeaders.length / 2]; // длина должна получиться как у первоначального заголовка, только что снятого с csv
        for (int i = 0; i < dataHeaders.length / 2; i++) {
            dataHeaders2[i] = dataHeadersFields[i] + " " + dataHeadersSqlTypes[i];
        }

        // ----- Используем Builder для Объединения в одну строку запроса - Здесь задается конечный формат ------
        // объединяем пары категория и тип в единый заголовок-строку для создания таблицы CREATE TABLE
        StringBuilder builder = new StringBuilder(); // используем его, чтоб не копировалась каждый раз строка при конкатенации с каждой итерацией
        for (int i = 0; i < dataHeaders2.length; i++) {
            if (i == dataHeaders2.length - 1) {
                builder.append(dataHeaders2[i]).append(""); // после последнего эл нет запятой
                break;
            }
            builder.append(dataHeaders2[i]).append(", ");

        }
        wholeHeader = builder.toString();

        // объединяем поля заголовка для команды INSERT
        StringBuilder builder1 = new StringBuilder();
        for (int i = 0; i < dataHeadersFields.length; i++) {
            if (i == dataHeadersFields.length - 1) {
                builder1.append(dataHeadersFields[i]).append("");
                break;
            }
            builder1.append(dataHeadersFields[i]).append(", ");
        }
        wholeHeaderFields = builder1.toString();

        // объединяем ячейки
        StringBuilder builder2 = new StringBuilder();
        for (int i = 0; i < dataCells.size(); i++) {
            // очередной ряд значений
            builder2.append("(");
            String[] tempArr = dataCells.get(i); // получаем из arraylist ряд

            for (int j = 0; j < dataCells.get(i).length; j++) {

                if (whereIsDate.contains(j)) { // Если из массива, в котором дата. Этот индекс совпадает с индексом из массива типов, потому что мы идем по тому же кол-ву столбцов
                    if (j == dataCells.get(i).length - 1) {
                        builder2.append("TO_DATE('").append(tempArr[j]).append("','dd.MM.yyyy')"); // если дата последняя
                        break;
                    }
                    builder2.append("TO_DATE('").append(tempArr[j]).append("','dd.MM.yyyy'), ");
                    continue;
                }
                if (whereIsFloat.contains(j)) {
                    if (j == dataCells.get(i).length - 1) {
                        builder2.append("'").append(tempArr[j].replace(",", ".")).append("'");
                        break;
                    }
                    builder2.append("'").append(tempArr[j].replace(",", ".")).append("', ");
                    continue;
                }
                // последняя ячейка
                if (j == dataCells.get(i).length - 1) {
                    builder2.append("'").append(tempArr[j]).append("'");
                    break;
                }
                builder2/*.replace("\"\"")*/.append("'").append(tempArr[j]).append("', ");
            }
            if (i == dataCells.size() - 1) {
                builder2.append(")");
                break;
            }
            builder2.append("),\n");
        }
        wholeCells = builder2.toString();


        queryString = "CREATE TABLE persons" + "(" + wholeHeader + ");\n" +
                "INSERT INTO persons"
                + "(" + wholeHeaderFields /*прям здесь можно было вставить вызов метода с циклом*/ + ") \nVALUES "
                + wholeCells + ";";


    }



}
